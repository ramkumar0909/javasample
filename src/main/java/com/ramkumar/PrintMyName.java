package com.ramkumar;

/**
 * Created by 1357536 on 5/12/2017.
 */
public class PrintMyName {

    public boolean print(String name) {
        if (name != null) {
            System.out.println(String.format("Hello %s", name));
            return true;
        } else {
            System.out.println("Testing coverage");
        }
        return false;
    }
}
