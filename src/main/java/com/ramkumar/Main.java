package com.ramkumar;

import com.ramkumar.exception.InvalidUsageException;
import com.ramkumar.exception.ThresholdLimitExceededException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by 1357536 on 5/18/2017.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        if (args == null || args.length != 2) {
            System.err.println("Usage java -jar <project>.jar <threshold_value_int> <actual_value_int>");
            throw new InvalidUsageException("Usage java -jar <project>.jar <threshold_value_int> <actual_value_int>");
        }
        int threshold = Integer.parseInt(args[0]);
        int value = Integer.parseInt(args[1]);
        generateReport();
        if (value < threshold) {
            System.out.println("Within the threshold value");
        } else {
            System.err.println("Not within the threshold limit");
            throw new ThresholdLimitExceededException("Not within the threshold limit");
        }
    }

    private static void generateReport() throws IOException {
        File file = new File("report.csv");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write("Testcase1, PASS\n");
        fileWriter.write("Testcase2, FAIL\n");
        fileWriter.write("Testcase3, PASS\n");
        fileWriter.close();
    }
}
