package com.ramkumar.exception;

/**
 * Created by 1357536 on 5/19/2017.
 */
public class InvalidUsageException extends Exception {
    public InvalidUsageException() {
        super();
    }

    public InvalidUsageException(String exception) {
        super(exception);
    }
}
