package com.ramkumar.exception;

/**
 * Created by 1357536 on 5/22/2017.
 */
public class ThresholdUnderTheLimitException extends  ThresholdLimitException {
    public ThresholdUnderTheLimitException() {
        super();
    }

    public ThresholdUnderTheLimitException(String exception) {
        super(exception);
    }
}
