package com.ramkumar.exception;

/**
 * Created by 1357536 on 5/19/2017.
 */
public class ThresholdLimitExceededException extends ThresholdLimitException {
    public ThresholdLimitExceededException() {
        super();
    }

    public ThresholdLimitExceededException(String exception) {
        super(exception);
    }
}
