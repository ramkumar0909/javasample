package com.ramkumar.exception;

/**
 * Created by 1357536 on 5/19/2017.
 */
public class ThresholdLimitException extends Exception {
    public ThresholdLimitException() {
        super();
    }

    public ThresholdLimitException(String exception) {
        super(exception);
    }
}
