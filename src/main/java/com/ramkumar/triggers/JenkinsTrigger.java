package com.ramkumar.triggers;

import com.ramkumar.exception.ThresholdLimitException;

/**
 * Created by 1357536 on 5/22/2017.
 */
public interface JenkinsTrigger {
    public boolean validate(int totalNumberOfExecution, int numberOfErrors, int thresholdPercentage) throws ThresholdLimitException;
}
