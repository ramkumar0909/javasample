package com.ramkumar.triggers;

import com.ramkumar.exception.ThresholdLimitExceededException;
import com.ramkumar.exception.ThresholdLimitException;
import com.ramkumar.exception.ThresholdUnderTheLimitException;

/**
 * Created by 1357536 on 5/22/2017.
 */
public class DefaultJenkinsTriggerImpl implements JenkinsTrigger {


    public boolean validate(int totalNumberOfExecution, int numberOfErrors, int thresholdPercentage) throws ThresholdLimitException {
        float numberOfSuccess = totalNumberOfExecution - numberOfErrors;
        float successRatio = numberOfSuccess / totalNumberOfExecution;
        float successPercentage = successRatio*100;
        if (successPercentage < thresholdPercentage) {
            throw new ThresholdUnderTheLimitException("Success rate is below the limit specified.");
        }
        return true;
    }
}
