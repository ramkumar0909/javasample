package com.ramkumar;

import com.ramkumar.exception.ThresholdLimitException;
import com.ramkumar.exception.ThresholdUnderTheLimitException;
import com.ramkumar.triggers.DefaultJenkinsTriggerImpl;
import com.ramkumar.triggers.JenkinsTrigger;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 1357536 on 5/22/2017.
 */
public class TestDefautlJenkinsTriggerImpl {

    @Test
    public void testValidateSuccess() throws ThresholdLimitException {
        JenkinsTrigger jenkinsTrigger = new DefaultJenkinsTriggerImpl();
        boolean isValid = jenkinsTrigger.validate(100, 30, 70);
        Assert.assertEquals(true, isValid);
    }

    @Test(expected = ThresholdUnderTheLimitException.class)
    public void testValidateFailure() throws ThresholdLimitException {
        JenkinsTrigger jenkinsTrigger = new DefaultJenkinsTriggerImpl();
        boolean isValid = jenkinsTrigger.validate(100, 40, 70);
    }
}
