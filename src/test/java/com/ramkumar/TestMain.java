package com.ramkumar;

import com.ramkumar.exception.InvalidUsageException;
import com.ramkumar.exception.ThresholdLimitExceededException;
import org.junit.Test;
import org.junit.rules.ExpectedException;

/**
 * Created by 1357536 on 5/18/2017.
 */
public class TestMain {

    @Test(expected=InvalidUsageException.class)
    public void testMainWithWrongArgs() throws Exception {
        String args[] = {};
        Main.main(args);
    }

    @Test
    public void testMainThresholdWithinLimit() throws Exception {
        String args[] = {"70","30"};
        Main.main(args);
    }

    @Test(expected=ThresholdLimitExceededException.class)
    public void testMainThresholdExceedLimits() throws Exception {
        String args[] = {"70", "90"};
        Main.main(args);
    }
}
