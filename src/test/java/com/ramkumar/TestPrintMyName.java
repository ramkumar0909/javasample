package com.ramkumar;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by 1357536 on 5/12/2017.
 */
public class TestPrintMyName {

    @Test
    public void testPrint() {
        String name = "Ramkumar";
        PrintMyName printMyName = new PrintMyName();
        boolean result = printMyName.print(name);
        Assert.assertEquals(true, result);
    }

    @Test
    public void testFailure() {
	    Assert.assertEquals(false, false);
    }
}
